import React from 'react'
import { StyleSheet, Text, View ,TextInput} from 'react-native'
import { colors } from '../../../utils/colors'

const Pass = ({placeholder}) => {
    return (

      <TextInput style={styles.input} placeholder={placeholder} placeholderTextColor={colors.default}  secureTextEntry={true}/>

    )
}

export default Pass;

const styles = StyleSheet.create({
    input:{
        borderWidth:1,
        borderColor:colors.default,
        borderRadius:25,
        paddingVertical:12,
        paddingHorizontal:18,
        fontSize:14,
        color :colors.default,
    }
})
