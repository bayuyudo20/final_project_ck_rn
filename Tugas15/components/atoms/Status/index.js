import React from 'react'
import { StyleSheet, Text, View ,TextInput} from 'react-native'
import { colors } from '../../../utils/colors'

const Status = ({name,value}) => {
    return (
        <View style={styles.container}>
            <Text>{name} : {value}</Text>
        </View>

    )
}

export default Status

const styles = StyleSheet.create({

    container:{
        backgroundColor:'white',
        padding: 17,
        marginHorizontal:30,
        borderRadius:10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,

        elevation: 10,
        //marginTop:5
    }

})
