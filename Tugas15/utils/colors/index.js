export const colors ={
    default:"#A55EEA",
    disable:"#A581c2",
    dark :"#474747",
    text :{
        default:'#7E7E7E',
    }
}