import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';

import HomeScreen from '../Pages/Home';
import AboutScreen from '../Pages/AboutScreen';
import AddScreen from '../Pages/AddScreen';
import PortalScreen from '../Pages/Portal';
import SettingScreen from '../Pages/Setting';
import WelcomeScreen from '../Pages/Welcome';
import RegisterScreen from '../Pages/Register';
import LoginScreen from '../Pages/Login';


const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

export default function Router() {
    return (
     <NavigationContainer>
            <Stack.Navigator initialRouteName="WelcomeScreen">
                    
                     <Stack.Screen name="WelcomeScreen" component={WelcomeScreen} options={{headerShown: false,}}/>
                      
                     <Stack.Screen name="Register" component={RegisterScreen} /> 
                    <Stack.Screen name="LoginScreen" component={LoginScreen} />
                     
                    <Stack.Screen name="MyDrawer" component={MyDrawer} />     
             </Stack.Navigator>
     </NavigationContainer>
    )
}
const MainApp=()=>(

        <Tab.Navigator>
            
            <Tab.Screen name="HomeScreen" component={HomeScreen} />
            <Tab.Screen name="AboutScreen" component={AboutScreen} />
            <Stack.Screen name="Register" component={RegisterScreen} /> 
            <Stack.Screen name="LoginScreen" component={LoginScreen} />
           
             
        </Tab.Navigator>


)
const MyDrawer=()=>(
    <Drawer.Navigator>
        <Drawer.Screen name="PortalScreen" component={PortalScreen} />
        <Drawer.Screen name="App" component={MainApp} />
        <Drawer.Screen name="AddScreen" component={AddScreen} />
        
    </Drawer.Navigator>
)
