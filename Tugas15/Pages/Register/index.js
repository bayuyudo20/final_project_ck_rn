import React from 'react';
import {View,Text,Image} from 'react-native';
import { Value } from 'react-native-reanimated';
import { Account } from '../../../assets';
import { Input,Button1, Pass } from '../../components';
import { colors } from '../../utils/colors';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';

const Register=()=>{
    return(
        <View style={styles.wrapper.page}>
            <ScrollView showsHorizontalScrollIndicator={false}>

                <Image source={Account} style={styles.illustration}/>
                <Text style={styles.text}>Mohon Mengisi data untuk proses pembuatan akun</Text>
                <View style={styles.space(64)}/>
                <Input placeholder="nama lengkap"/>
                <View style={styles.space(33)}/>
                <Input placeholder="email"/>
                <View style={styles.space(33)}/>
                <Pass placeholder="Password"/>
                <View style={styles.space(33)}/>
                <Pass placeholder="konfirmasi password"/>
                <View style={styles.space(33)}/>
                <Button1 title="Register"/>
            </ScrollView>
        </View>
    )
}

const styles={
    wrapper:{
        
        page:{
            padding:20,
            backgroundColor:'white',
            flex:1,
        },
        
    },

    illustration:{
        width:106,
        height:115,
        marginTop:8,
    },
    text:{
        fontSize:14,
        fontWeight:'bold',
        color:colors.default,
        marginTop:16,
        maxWidth:200
    },
    space: value => {
        return{
            height:value,
        }   
    }
}

export default Register;