import React from 'react';
import {View,Text,Image} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Value } from 'react-native-reanimated';
import { ImgLogin, masuk, WelcomeAuth } from '../../../assets';
import { Button1, Input, Pass } from '../../components';
import { colors } from '../../utils/colors';

const Login=({navigation})=>{
    const handleGoTo =(screen)=>{
        navigation.navigate(screen)
    }
    return(
        <View style={styles.wrapper.page}> 
            <ScrollView>
                
                <Image source={masuk} style={styles.illustration}/>
                <Text style={styles.text}>Mohon Mengisi data untuk Login</Text>
                <View style={styles.space(64)}/>
                <Input placeholder="email"/>
                <View style={styles.space(33)}/>
                <Pass  placeholder="password"/>
                <View style={styles.space(33)}/>
                <Button1 title="Login" onPress={ ()=> handleGoTo('MyDrawer')} />
            </ScrollView>

        </View>
    )
}

const styles={
    wrapper:{
        
        page:{
            padding:20,
            backgroundColor:'white',
            flex:1,
        },
        
    },

    illustration:{
        width:190,
        height:130,
        marginTop:8,
    },
    text:{
        fontSize:14,
        fontWeight:'bold',
        color:colors.default,
        marginTop:16,
        maxWidth:200
    },
    space: value => {
        return{
            height:value,
        }   
    }
}

export default Login;