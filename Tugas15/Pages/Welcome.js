import { setStatusBarBackgroundColor } from 'expo-status-bar'
import React from 'react'
import {StyleSheet, Text,View,Button,Image} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { ImgLogin, WelcomeAuth } from '../../assets'
import { colors } from '../utils/colors'
import ActionButton from './ActionButton'

const Welcome= ({navigation}) =>{
    const handleGoTo =(screen)=>{
        navigation.navigate(screen)
    }
    return(
        <View style={styles.wrapper.page}>      
                <Image source={WelcomeAuth} style={styles.wrapper.illustration}/>
                <Text style={styles.text.welcome}>Selamat Datang
                </Text>
            <ActionButton desc="Silakan masuk jika anda telah memiliki akun" title="Masuk" onPress={ ()=> handleGoTo('LoginScreen')}/>
            <ActionButton desc="Silakan daftar jika belom punya akun" title="Daftar" onPress={()=> handleGoTo('Register')}/>
        </View>
    )
}
export default Welcome;

const styles={
    wrapper:{
        page:{
            alignItems:'center', 
            justifyContent:'center',
            flex:1,
            backgroundColor:'white'
        },
        illustration :{
            width:219,
            height:117,
            marginBottom:10
        }
    },
    text:{
        welcome:{
            fontSize:18,
            fontWeight:'bold',
            color:colors.default,
            marginBottom:76}
    }
}