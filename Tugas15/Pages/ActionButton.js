import { setStatusBarBackgroundColor } from 'expo-status-bar'
import React from 'react'
import {StyleSheet, Text,View,Button} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Button1 } from '../components'
import { colors } from '../utils/colors'

const ActionButton =({desc,title,onPress})=>{
    
    return(
        <View style={styles.wrapper.Comment}>
            <Text style={styles.text.desc}>{desc}</Text>
            <Button1 title={title} onPress={onPress}/>
        </View>
    )
}

const styles ={
    wrapper:{
        Comment: {marginBottom:43,maxWidth:225}
    },
    text:{
        desc:{
            fontSize:10,
            color: colors.text.default,
            textAlign:'center',
            paddingHorizontal:'15%',
            marginBottom:6

        }
    }
}

export default ActionButton;