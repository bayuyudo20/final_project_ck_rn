import React,{useState,useEffect} from 'react'
import {StyleSheet, Text,View,Button,Image} from 'react-native'
import { Button1, Status } from '../components'
import { ImgLogin, masuk, WelcomeAuth,covid } from '../../assets'

export default function Home(){
    
    const [ dataCovid, setDataCovid ] = useState({
        dirawat:'',
        meninggal:'',
        name:'',
        positif:'',
        sembuh:'',
     
    });

    useEffect(() => {
       getData();
    }, []);

    const getData =()=>{
        fetch('https://api.kawalcorona.com/indonesia')
        .then(response => response.json())
        .then(json =>{
            console.log(json)
            setDataCovid(json[0])
        })
    }
    return(
        <View style={styles.container}>
            <Image source={covid} style={styles.illustration}/>
            <View style={styles.space(10)}/>
            <Status name='Negara' value={dataCovid.name}/>
            <View style={styles.space(10)}/>
            <Status name='Meninggal' value={dataCovid.meninggal}/>
            <View style={styles.space(10)}/>
            <Status name='Positif' value={dataCovid.positif}/>
            <View style={styles.space(10)}/>
            <Status name='Sembuh' value={dataCovid.sembuh}/>
        </View>
    )
}

const styles=StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems : 'center',
        backgroundColor:'white',
    },
    space: value => {
        return{
            height:value,
        }   
    },
    illustration:{
        width:190,
        height:130,
        marginTop:2,
    },
})