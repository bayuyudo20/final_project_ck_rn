import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Router from './Router'
export * from './utils/colors'

export default function index() {
    return (
        <Router/>
    )
}

const styles = StyleSheet.create({})
